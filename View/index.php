<?php
	require('../Model/Database.php');
	require('layouts/header.php');
	require('../Controller/WorkController.php');
	require('../Model/WorkModel.php');

	if (class_exists('Database')) {
		$db = new Database();
	}

	if (class_exists('WorkController')) {
		$workController = new WorkController();

		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = '';
		}
		if ($action == 'add') {
			$workController->addWorkController();
		} elseif ($action == 'edit') {
			$workController->editWorkController();
		} elseif ($action == 'delete') {
			$workController->deleteWorkController();
		} else {
			$workController->listWorkController();
		}
	}



	require('layouts/footer.php');
?>