
<div class="content-wrapper" style="min-height: 353px;">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Thêm công việc</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <a href="index.php" class="btn btn-primary float-right">Trở về</a>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <form method="post">
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row form-group">
                                <label class="col-form-label">Tên công việc<span class="text-danger">*</span></label>
                                <?php 
                                if (isset($workModel->_errors['name'])) {
                                ?>
                                    <span class="text-danger"><?=$workModel->_errors['name']?></span>
                                <?php 
                                }
                                ?>
                                <input type="text" name="name" class="form-control" placeholder="Tên công việc" value="<?= $workModel->name ? $workModel->name :''?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="row form-group">
                                <label class="col-form-label">Ngày bắt đầu<span class="text-danger">*</span></label>
                                <?php 
                                if (isset($workModel->_errors['startDate'])) {
                                ?>
                                    <span class="text-danger"><?=$workModel->_errors['startDate']?></span>
                                <?php 
                                }
                                ?>
                                <input type="text" id="startDate" data-toggle="datetimepicker" name="startDate" class="form-control datetimepicker-input" placeholder="Năm/Tháng/Ngày" maxlength="10" value="<?= $workModel->startDate ? $workModel->startDate :''?>" required>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-sm-5">
                            <div class="row form-group">
                                <label class="col-form-label">Ngày kết thúc<span class="text-danger">*</span></label>
                                <?php 
                                if (isset($workModel->_errors['endDate'])) {
                                ?>
                                    <span class="text-danger"><?=$workModel->_errors['endDate']?></span>
                                <?php 
                                }
                                ?>
                                <input type="text" id="endDate" data-toggle="datetimepicker" name="endDate" class="form-control datetimepicker-input" placeholder="Năm/Tháng/Ngày" maxlength="10" value="<?= $workModel->endDate ? $workModel->endDate :''?>" required>
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="row form-group">
                                <label class="col-form-label">Trạng thái<span class="text-danger">*</span></label>
                                <?php 
                                if (isset($workModel->_errors['status'])) {
                                ?>
                                    <span class="text-danger"><?=$workModel->_errors['status']?></span>
                                <?php 
                                }
                                ?>
                                <select class="custom-select" name="status" required>
                                  <option selected>--- Trạng thái ---</option>
                                  <option value="1" <?= $workModel->status == 1 ? 'selected' :''?>>Lập kế hoạch</option>
                                  <option value="2" <?= $workModel->status == 2 ? 'selected' :''?>>Đang tiến hành</option>
                                  <option value="3" <?= $workModel->status == 3 ? 'selected' :''?>>Hoàn thành</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="addWork" class="btn btn-success">Thêm mới</button>
                </div>
            </form>
        </div>
    </section>
</div>