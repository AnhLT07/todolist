<div class="content-wrapper" style="min-height: 365px;">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">DANH SÁCH CÔNG VIỆC</h1>
				</div><!-- /.col -->
				<div class="col-sm-6 float-sm-right">
					<a href="?action=add" class="btn btn-primary float-right">Thêm mới</a>
				</div>
			</div>
		</div>
	</div>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<!--  -->
				<div class="response"></div>
    			<div id='calendar'></div>
    			<input type="hidden" id="jsonWorkList" value='<?php if (!empty($listWork)) { echo $listWork; }else{ echo ''; }?>'>
			</div>
		</div>
	</section>
</div>
<!-- modal -->
<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="event-details-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content rounded-0">
            <div class="modal-header rounded-0">
                <h5 class="modal-title">Chi tiết công việc</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body rounded-0">
                <div class="container-fluid">
				  	<div class="row">
					    <div class="col-sm-12">
					    	<label class="text-muted">Tên công việc: </label>
					    	<span id="title"></span>
					    </div>
					</div>
					<div class="row">
					    <div class="col-sm-6">
					    	<label class="text-muted">Ngày bắt đầu: </label>
					    	<span id="start"></span>
					    </div>
					    <div class="col-sm-6">
					    	<label class="text-muted">Ngày kết thúc: </label>
					    	<span id="end"></span>
					    </div>
					</div>
					<div class="row">
					    <div class="col-sm-12">
					    	<label class="text-muted">Trạng thái: </label>
					    	<span id="status"></span>
					    </div>
					</div>
                </div>
            </div>
            <div class="modal-footer rounded-0">
                <div class="text-end">
                    <button type="button" class="btn btn-primary btn-sm rounded-0" id="edit" data-id="">Sửa</button>
                    <button type="button" class="btn btn-danger btn-sm rounded-0" id="delete" data-id="">Xóa</button>
                    <button type="button" class="btn btn-secondary btn-sm rounded-0" data-bs-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</div>