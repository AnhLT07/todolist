<footer class="main-footer text-center">
    <strong>Copyright &copy; 2022.</strong>
    All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

<!-- jQuery -->
<script src="../Public/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../Public/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../Public/plugins/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<!-- ChartJS -->
<script src="../Public/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../Public/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../Public/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../Public/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../Public/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../Public/plugins/moment/moment.min.js"></script>
<script src="../Public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../Public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../Public/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../Public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../Public/dist/js/adminlte.js"></script>
<script src="../Public/calendar/fullcalendar.min.js"></script>
<script src="https://unpkg.com/imask"></script>
<script type="text/javascript">
    
    $('#startDate, #endDate').datetimepicker({
        format: 'YYYY/MM/DD',
    })
    //
    $(function() {
        var list = $("#jsonWorkList").val();
        if(typeof list !== 'undefined'){
            var scheds = $.parseJSON(list);
            var calendar;
            var Calendar = FullCalendar.Calendar;
            var events = [];


            if (!!scheds) {
                Object.keys(scheds).map(k => {
                    var row = scheds[k]
                    var date = new Date(row.ending_date);

                    // Add a day
                    date.setDate(date.getDate() + 1);
                    date.toLocaleDateString();
                    endDate = moment(date).format('yyyy-MM-DD');
                    if(row.status == 1){
                        colorTitle = "#6599FF";
                    }else if(row.status == 2){
                        colorTitle = "#FF9900";
                    }else{
                        colorTitle = "#55D43F";
                    }
                    events.push({ id: row.id, title: row.work_name, start: row.starting_date, end: endDate, color: colorTitle});
                });
            }
            calendar = new Calendar(document.getElementById('calendar'),{
                headerToolbar: {
                    left: 'prev,next today',
                    right: 'dayGridMonth,dayGridWeek,dayGridDay',
                    center: 'title',
                },
                initialView: 'dayGridDay',
                selectable: true,
                themeSystem: 'bootstrap',
                events: events,
                eventClick: function(info) {
                    var details = $('#event-details-modal');
                    var id = info.event.id;

                    if (!!scheds[id]) {
                        if(scheds[id].status == 1){
                            statusWork = "Lập kế hoạch";
                        }else if(scheds[id].status == 2){
                            statusWork = "Đang tiến hành";
                        }else{
                            statusWork = "Hoàn thành";
                        }
                        details.find('#title').text(scheds[id].work_name);
                        details.find('#start').text(scheds[id].starting_date);
                        details.find('#end').text(scheds[id].ending_date);
                        details.find('#status').text(statusWork);
                        details.find('#edit,#delete').attr('data-id', id);
                        details.modal('show');
                    } else {
                        alert("Event is undefined");
                    }
                },
                editable: true
            });
            calendar.render();
            //
            if (!!scheds){
                // Form reset listener
                $('#schedule-form').on('reset', function() {
                    $(this).find('input:hidden').val('');
                    $(this).find('input:visible').first().focus();
                });

                // Edit Button
                $('#edit').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = 'index.php?action=edit&id='+id;
                });

                // Delete Button / Deleting an Event
                $('#delete').click(function() {
                    var id = $(this).attr('data-id');

                    if (!!scheds[id]) {
                        var _conf = confirm("Bạn chắc chắn muốn xóa không");
                        if (_conf === true) {
                            location.href = "index.php?action=delete&id=" + id;
                        }
                    } else {
                        alert("Event is undefined");
                    }
                });
            }
        }
        
    });

    function dateMask(dateMask){
        var dateMask = IMask(dateMask,{
            mask: Date,  // enable date mask

            pattern: 'Y/`m/`d',  
            blocks: {
                d: {
                    mask: IMask.MaskedRange,
                    from: 1,
                    to: 31,
                    maxLength: 2,
                },
                m: {
                    mask: IMask.MaskedRange,
                    from: 1,
                    to: 12,
                    maxLength: 2,
                    },
                Y: {
                    mask: IMask.MaskedRange,
                    from: 1900,
                    to: 9999,
                    }
                },
            // define date -> str convertion
            format: function (date) {
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();

                if (day < 10) day = "0" + day;
                if (month < 10) month = "0" + month;

                return [year, month, day].join('/');
            },
            // define str -> date convertion
            parse: function (str) {
                var yearMonthDay = str.split('/');
                return new Date(yearMonthDay[0], yearMonthDay[1] - 1, yearMonthDay[2]);
            },

            // optional interval options
            min: new Date(1900, 12, 1),  // defaults to `1900-01-01`
            max: new Date(9999, 12, 1),  // defaults to `9999-01-01`
        });
    }
    dateMask(document.getElementById('startDate'));
    dateMask(document.getElementById('endDate'));
    //
    
</script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="../Public/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="../Public/dist/js/demo.js"></script> -->
</body>
</html>