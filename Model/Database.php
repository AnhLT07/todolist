<?php 
class Database
{
	public $conn = NULL;
	private $server = 'localhost';
	private $dbName = 'todolist';
	private $user = 'root';
	private $password = '';
        
	public function connect(){
		$this->conn = null;
		try{
			$this->conn = new PDO("mysql:host=".$this->server.";dbname=".$this->dbName."", $this->user, $this->password);

			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e) {
		  	echo $e->getMessage();
		}
	}

    public function closeDatabase(){
		if ($this->conn) {
			$this->conn = null;
		}
	}
}
?>