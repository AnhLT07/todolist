<?php 
class WorkModel extends Database {
	protected $db;
	public $_errors = array();
	public $name;
    public $startDate;
    public $endDate;
    public $status;

	public function __construct()
	{
		$this->db = new Database();
		$this->db->connect();
	}

	public function addError($attribute, $error)
	{
		$this->_errors[$attribute] = $error;
	}

	public function load($dataName, $dataStart, $dataEnd, $dataStatus)
	{
		$this->name = $dataName;
		$this->startDate = $dataStart;
		$this->endDate = $dataEnd;
		$this->status = $dataStatus;
	}

	public function validate()
	{
		if (empty(trim($this->name))) {
			$this->addError('name', 'Vui lòng nhập tên công việc!');
		}
		if (empty($this->startDate)) {
			$this->addError('startDate', 'Vui lòng chọn ngày bắt đầu!');
		}
		if (empty($this->endDate)) {
			$this->addError('endDate', 'Vui lòng chọn ngày kết thúc!');
		}
		if (empty($this->status)) {
			$this->addError('status', 'Vui lòng chọn trạng thái công việc!');
		}
		if ($this->startDate > $this->endDate) {
			$this->addError('endDate', 'Ngày kết thúc phải sau ngày bắt đầu!');
		}
		if (count($this->_errors) > 0) {
			return false;
		}
		return true;
	}

	public function addWork($name, $start, $end, $status)
	{	
		$sql = "INSERT INTO work (work_name, starting_date, ending_date, status)
							VALUES ('{$name}', '{$start}', '{$end}', '{$status}')";
		return $this->db->conn->prepare($sql);
	}

	public function getWork($workId)
	{
		$sql = "SELECT * FROM work WHERE id = $workId";
		$result = $this->db->conn->prepare($sql);
		$result->execute();
		$data = $result->fetch(PDO::FETCH_ASSOC);

		return $data;
	}

	public function editWork($name, $start, $end, $status, $workId)
	{
		$sql = "UPDATE work SET work_name = '{$name}', starting_date = '{$start}', ending_date = '{$end}', status = '{$status}' WHERE id = '{$workId}'";
		
		return $this->db->conn->prepare($sql);
	}

	public function getListWork()
	{
		$sql = "SELECT * FROM work";
		$result = $this->db->conn->prepare($sql);
		$result->execute();
		$resultArray = [];

        foreach($result->fetchAll() as $row){
            $row['sdate'] = date("F d, Y h:i A",strtotime($row['starting_date']));
            $row['edate'] = date("F d, Y h:i A",strtotime($row['ending_date']));
            $resultArray[$row['id']] = $row;
        }

		return json_encode($resultArray);
	}
	
	public function deleteWork($delId)
	{
		$sql = "DELETE FROM work WHERE id = '{$delId}'";
		
		return $this->db->conn->prepare($sql);
	}

}
?>