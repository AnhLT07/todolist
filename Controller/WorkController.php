<?php 
class WorkController {

	public function listWorkController()
	{
		$workModel = new WorkModel();
		$listWork = $workModel->getListWork();
		require('home.php');
	}

	public function addWorkController()
	{
		$workModel = new WorkModel();
		if(isset($_POST['addWork'])){

			$workModel->load(trim($_POST['name']), $_POST['startDate'], $_POST['endDate'], $_POST['status']);
			if($workModel->validate()){
				$stmt = $workModel->addWork($workModel->name, $workModel->startDate, $workModel->endDate, $workModel->status);
				$stmt->execute();
				echo "<script>
						alert('Thêm mới thành công');
						window.location.href='index.php';
					</script>";

			}
		}

		require('work/add-work.php');
	}

	public function editWorkController()
	{
		$workModel = new WorkModel();
		if (isset($_GET['id'])) {
			
			$workId = $_GET['id'];
			$workOld = $workModel->getWork($workId);
			if(isset($_POST['editWork'])){

				$workModel->load(trim($_POST['name']), $_POST['startDate'], $_POST['endDate'], $_POST['status']);
				if($workModel->validate()){

					$stmt = $workModel->editWork($workModel->name, $workModel->startDate, $workModel->endDate, $workModel->status, $workId);
					$stmt->execute();
					echo "<script>
						alert('Cập nhật thành công');
						window.location.href='index.php';
					</script>";
				}
			}
		}
		require('work/edit-work.php');

	}
	
	public function deleteWorkController()
	{
		$del = $_GET['id'];
		$workModel = new WorkModel();
		$stmt = $workModel->deleteWork($del);
		$stmt->execute();
		header('Location: index.php');

		require('home.php');
	}

}
?>